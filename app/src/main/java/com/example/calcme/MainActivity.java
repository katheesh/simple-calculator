package com.example.calcme;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {

    Button addbtn, Clear;

    EditText EditText1, EditText2, ResultBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText1 = (EditText) findViewById(R.id.editText1);
        EditText2 = (EditText) findViewById(R.id.editText2);
        ResultBox = (EditText) findViewById(R.id.Result);

        addbtn = (Button) findViewById(R.id.addbtn);
        Clear = (Button) findViewById(R.id.Clear);

        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double numb1 = Double.parseDouble(EditText1.getText().toString());
                double numb2 = Double.parseDouble(EditText2.getText().toString());

                double total = numb1 + numb2;

                ResultBox.setText(String.valueOf(total));
            }
        });

        Clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText1.setText("");
                EditText2.setText("");
                ResultBox.setText("");
            }
        });
    }
}
